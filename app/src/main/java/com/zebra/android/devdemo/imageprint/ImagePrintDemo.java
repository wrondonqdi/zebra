/***********************************************
 * CONFIDENTIAL AND PROPRIETARY 
 * 
 * The source code and other information contained herein is the confidential and the exclusive property of
 * ZIH Corp. and is subject to the terms and conditions in your end user license agreement.
 * This source code, and any other information contained herein, shall not be copied, reproduced, published, 
 * displayed or distributed, in whole or in part, in any medium, by any means, for any purpose except as
 * expressly permitted under such license agreement.
 * 
 * Copyright ZIH Corp. 2012
 * 
 * ALL RIGHTS RESERVED
 ***********************************************/

package com.zebra.android.devdemo.imageprint;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.zebra.android.devdemo.R;
import com.zebra.android.devdemo.util.SettingsHelper;
import com.zebra.android.devdemo.util.UIHelper;
import com.zebra.sdk.comm.BluetoothConnection;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.comm.TcpConnection;
import com.zebra.sdk.device.ZebraIllegalArgumentException;
import com.zebra.sdk.graphics.internal.ZebraImageAndroid;
import com.zebra.sdk.printer.PrinterLanguage;
import com.zebra.sdk.printer.ZebraPrinter;
import com.zebra.sdk.printer.ZebraPrinterFactory;
import com.zebra.sdk.printer.ZebraPrinterLanguageUnknownException;

public class ImagePrintDemo extends Activity {

    private RadioButton btRadioButton;
    private EditText macAddressEditText;
    private EditText ipAddressEditText;
    private EditText portNumberEditText;
    private EditText printStoragePath;
    private static final String bluetoothAddressKey = "ZEBRA_DEMO_BLUETOOTH_ADDRESS";
    private static final String tcpAddressKey = "ZEBRA_DEMO_TCP_ADDRESS";
    private static final String tcpPortKey = "ZEBRA_DEMO_TCP_PORT";
    private static final String PREFS_NAME = "OurSavedAddress";
    private UIHelper helper = new UIHelper(this);
    private static int TAKE_PICTURE = 1;
    private static int PICTURE_FROM_GALLERY = 2;
    private static File file = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.image_print_demo);
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);

        ipAddressEditText = (EditText) this.findViewById(R.id.ipAddressInput);
        String ip = settings.getString(tcpAddressKey, "");
        ipAddressEditText.setText(ip);

        portNumberEditText = (EditText) this.findViewById(R.id.portInput);
        String port = settings.getString(tcpPortKey, "");
        portNumberEditText.setText(port);

        macAddressEditText = (EditText) this.findViewById(R.id.macInput);
        String mac = settings.getString(bluetoothAddressKey, "");
        macAddressEditText.setText(mac);

        printStoragePath = (EditText) findViewById(R.id.printerStorePath);

        CheckBox cb = (CheckBox) findViewById(R.id.checkBox);
        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    printStoragePath.setVisibility(View.VISIBLE);
                } else {
                    printStoragePath.setVisibility(View.INVISIBLE);
                }
            }
        });

        btRadioButton = (RadioButton) this.findViewById(R.id.bluetoothRadio);

        Button cameraButton = (Button) this.findViewById(R.id.testButton);
        cameraButton.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                getPhotoFromCamera();
            }
        });

        Button galleryButton = (Button) this.findViewById(R.id.galleryButton);
        galleryButton.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                getPhotosFromGallery();
            }
        });

        Button resetButton = (Button) this.findViewById(R.id.button3);
        cameraButton.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                resetPrinter();
            }
        });

        RadioGroup radioGroup = (RadioGroup) this.findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.bluetoothRadio) {
                    toggleEditField(macAddressEditText, true);
                    toggleEditField(portNumberEditText, false);
                    toggleEditField(ipAddressEditText, false);
                } else {
                    toggleEditField(portNumberEditText, true);
                    toggleEditField(ipAddressEditText, true);
                    toggleEditField(macAddressEditText, false);
                }
            }
        });
    }

    private void resetPrinter() {
        Connection connection = getZebraPrinterConn();
        try {
            connection.open();
            ZebraPrinter printer = ZebraPrinterFactory.getInstance(connection);
            printer.reset();
            connection.close();
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (ZebraPrinterLanguageUnknownException e) {
            e.printStackTrace();
        }
    }

    private void getPhotosFromGallery() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PICTURE_FROM_GALLERY);
    }

    private void toggleEditField(EditText editText, boolean set) {
        /*
         * Note: Disabled EditText fields may still get focus by some other means, and allow text input.
         *       See http://code.google.com/p/android/issues/detail?id=2771
         */
        editText.setEnabled(set);
        editText.setFocusable(set);
        editText.setFocusableInTouchMode(set);
    }

    private boolean isBluetoothSelected() {
        return btRadioButton.isChecked();
    }

    private String getMacAddressFieldText() {
        return macAddressEditText.getText().toString();
    }

    private String getTcpAddress() {
        return ipAddressEditText.getText().toString();
    }

    private String getTcpPortNumber() {
        return portNumberEditText.getText().toString();
    }

    private void getPhotoFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = new File(Environment.getExternalStorageDirectory(), "tempPic.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
        startActivityForResult(intent, TAKE_PICTURE);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == TAKE_PICTURE) {
                printPhotoFromExternal(BitmapFactory.decodeFile(file.getAbsolutePath()));
            }
            if (requestCode == PICTURE_FROM_GALLERY) {
                Uri imgPath = data.getData();
                Bitmap myBitmap = null;
                try {
                    myBitmap = Media.getBitmap(getContentResolver(), imgPath);
                } catch (FileNotFoundException e) {
                    helper.showErrorDialog(e.getMessage());
                } catch (IOException e) {
                    helper.showErrorDialog(e.getMessage());
                }
                printPhotoFromExternal(myBitmap);
            }
        }
    }

    private void printPhotoFromExternal(final Bitmap bitmap) {
        new Thread(new Runnable() {
            public void run() {
                try {
                    getAndSaveSettings();

                    Looper.prepare();
                    helper.showLoadingDialog("Sending image to printer");
                    Connection connection = getZebraPrinterConn();
                    connection.open();
                    ZebraPrinter printer = ZebraPrinterFactory.getInstance(connection);

                    if (((CheckBox) findViewById(R.id.checkBox)).isChecked()) {
                        /////////////printer.storeImage(printStoragePath.getText().toString(), new ZebraImageAndroid(bitmap), 550, 412);

                    } else {

                        /*


                        String cpclConfigLabel="! U1 setvar \"device.languages\" \"ZPL\"\r\n";
                        connection.write(cpclConfigLabel.getBytes());
                        if (printer.getPrinterControlLanguage()== PrinterLanguage.ZPL) {
                            printer.sendCommand("^XA\r\n^POI\r\n^MNN\r\n^LL550^XZ\r\n");
                        }
                        cpclConfigLabel="! U1 setvar \"device.languages\" \"line_print\"\r\n";
                        connection.write(cpclConfigLabel.getBytes());
                    	cpclConfigLabel = "! 0 200 200 406 1\r\n" + "ON-FEED IGNORE\r\n" + "BOX 20 20 380 380 8\r\n" + "T 0 6 137 177 TEST222\r\n" + "PRINT\r\n";
                    	connection.write(cpclConfigLabel.getBytes());
                    	                    	


                    	cpclConfigLabel = "! 0 200 200 210 1\r\n" + "TEXT 4 0 30 40 Hello World\r\n" +  "PRINT\r\n";
                    	connection.write(cpclConfigLabel.getBytes());

                    	cpclConfigLabel = "! UTILITIES\r\n" + "IN-MILLIMETERS\r\n" + "SETFF 0 0\r\n" + "PRINT\r\n";
                    	connection.write(cpclConfigLabel.getBytes());

                        cpclConfigLabel = "! U1 setvar \"media.tof\" \"0\" \r\n";
                        connection.write(cpclConfigLabel.getBytes());

                        cpclConfigLabel = "! U1 setvar \"media.type\" \"journal\" \r\n";
                        connection.write(cpclConfigLabel.getBytes());


                    	cpclConfigLabel = "! UTILITIES\r\n" + "SET-TOF 0\r\n" + "PRINT\n";
                    	connection.write(cpclConfigLabel.getBytes());

                        printer.printImage(new ZebraImageAndroid(bitmap), 0, 0, 550, 412, false);
                        connection.write(cpclConfigLabel.getBytes());
                        
                        cpclConfigLabel = "! 0 200 200 210 1\r\n" + "TEXT 4 0 30 40 Hello World222\r\n" +  "PRINT\r\n";
                    	connection.write(cpclConfigLabel.getBytes());
                    	
                    	cpclConfigLabel = "! 0 200 200 210 1\r\n" + "TEXT 4 0 30 40 Hello World333\r\n" +  "PRINT\r\n";
                    	connection.write(cpclConfigLabel.getBytes());

                    	printer.printImage(new ZebraImageAndroid(bitmap), 0, 0, 300, 300, false);

                        cpclConfigLabel = "! 0 200 200 210 1\r\n" + "TEXT 4 0 30 40 Hello World4444\r\n"  + "PRINT\r\n";
                        connection.write(cpclConfigLabel.getBytes());

                        printer.printImage(new ZebraImageAndroid(bitmap), 0, 0, 200, 200, false);

                        cpclConfigLabel = "! 0 200 200 210 1\r\n" + "TEXT 4 0 30 40 Hello World5555\r\n"  + "PRINT\r\n";
                        connection.write(cpclConfigLabel.getBytes());

                            */
                        String cpclConfigLabel = "! 0 200 200 210 1\r\n" + "TEXT 4 0 30 40 Hello World5555\r\n"  + "PRINT\r\n";
                        connection.write(cpclConfigLabel.getBytes());
                        String aux = "! UTILITIES\r\n" + "IN-MILLIMETERS\r\n" + "SETFF 0 0\r\n" + "PRINT\r\n";
                        connection.write(aux.getBytes());
                        cpclConfigLabel = "! U1 X 158\r\n"+
                        "! U1 SETLF 100\r\n"+
                        "! U1 SETLF 20\r\n"+
                        "! U1 LMARGIN 25\r\n"+
                        "! U1 SETLP UBUNTUMO.CPF 0 9\r\n"+
                        "! 0 200 200 25 1\r\n"+
                        "CENTER\r\n"+
                        "TEXT 5 0 0 0 On Behalf Of\r\n"+
                        "PRINT\r\n"+
                        "! U1 SETLF 20\r\n"+
                        "! U1 LMARGIN 25\r\n"+
                        "! U1 SETLP UBUNTUMO.CPF 0 9\r\n"+
                        "! 0 200 200 45 1\r\n"+
                        "CENTER\r\n"+
                        "TEXT 5 0 0 0 Hess Trading Corp\r\n"+
                        "PRINT\r\n"+
                        "! U1 SETLF 20\r\n"+
                        "! U1 LMARGIN 25\r\n"+
                        "! U1 SETLP UBUNTUMO.CPF 0 9\r\n"+
                        "! U1 X 75\r\n"+
                        "! U1 SETBOLD 1 Emergency Contact #:! U1 SETBOLD 0  1-800-SKY-TANK\r\n"+
                        "! U1 X 5\r\n"+
                        "UN1267;Petroleum Crude Oil;3;PGI\r\n"+
                        "! 0 200 200 10 1\r\n"+
                        "LINE 0 0 560 0 2\r\n"+
                        "LINE 0 6 560 6 2\r\n"+
                        "PRINT\r\n"+
                        "! U1 SETLF 20\r\n"+
                        "! U1 SETBOLD 1 Ticket #:! U1 SETBOLD 0  aadmin19\r\n"+
                        "! U1 SETLF 7\r\n"+
                        "! U1 SETBOLD 1 Customer Ref #:! U1 SETBOLD 0  234\r\n"+
                        "! 0 200 200 5 1\r\n"+
                        "LINE 0 0 560 0 2\r\n"+
                        "PRINT\r\n"+
                        "! U1 SETLF 20\r\n"+
                        "! U1 SETBOLD 1 Driver Name:! U1 SETBOLD 0  Test User\r\n"+
                        "! U1 SETBOLD 1 Carrier:! U1 SETBOLD 0  QCER! U1 X 258 ! U1 SETBOLD 1 Driver:! U1 SETBOLD 0  aadmin\r\n"+
                        "! U1 SETLF 7\r\n"+
                        "! U1 SETBOLD 1 Truck #:! U1 SETBOLD 0  209! U1 X 258 ! U1 SETBOLD 1 Trailer #:! U1 SETBOLD 0  105109\r\n"+
                        "! 0 200 200 5 1\r\n"+
                        "LINE 0 0 560 0 2\r\n"+
                        "PRINT\r\n"+
                        "! U1 SETLF 20\r\n"+
                        "! U1 X 180\r\n"+
                        "! U1 SETBOLD 1 Pickup Location\r\n"+
                        "! U1 SETBOLD 1 Name:! U1 SETBOLD 0  AN-Evenson 152-95-0310H-3\r\n"+
                        "! U1 SETBOLD 1 County:! U1 SETBOLD 0  McKenzie! U1 X 258 ! U1 SETBOLD 1 State:! U1 SETBOLD 0  ND\r\n"+
                        "! U1 SETBOLD 1 Producer:! U1 SETBOLD 0  HESS CORPORATION\r\n"+
                        "! U1 SETBOLD 1 Location:! U1 SETBOLD 0  LOT3 3-152-95\r\n"+
                        "! U1 SETLF 7\r\n"+
                        "! U1 SETBOLD 1 Time:! U1 SETBOLD 0  08/15/2014 09:30:23 AM\r\n"+
                        "! 0 200 200 5 1\r\n"+
                        "LINE 0 0 560 0 2\r\n"+
                        "PRINT\r\n"+
                        "! U1 SETLF 20\r\n"+
                        "! U1 SETBOLD 1 Gross Barrels:! U1 SETBOLD 0  40.01\r\n"+
                        "! U1 SETBOLD 1 Tank/Meter #:! U1 SETBOLD 0  89\r\n"+
                        "! U1 SETBOLD 1 Tank Size:! U1 SETBOLD 0  300/15 (1.6667)\r\n"+
                        "! U1 SETBOLD 1 T.Gauge:! U1 SETBOLD 0  6'0\" 0/4\"! U1 X 258 ! U1 SETBOLD 1 B.Gauge:! U1 SETBOLD 0  4'0\" 0/4\"\r\n"+
                        "! U1 SETBOLD 1 T.Temp:! U1 SETBOLD 0  6! U1 X 258 ! U1 SETBOLD 1 B.Temp:! U1 SETBOLD 0  4\r\n"+
                        "! U1 SETBOLD 1 T.BS&W:! U1 SETBOLD 0  0' 0\"! U1 X 258 ! U1 SETBOLD 1 B.BS&W:! U1 SETBOLD 0  0' 0\"\r\n"+
                        "! U1 SETLF 20\r\n"+
                        "! U1 SETBOLD 1 Obs. Gravity:! U1 SETBOLD 0  25! U1 X 258 ! U1 SETBOLD 1 Obs. Temp:! U1 SETBOLD 0  25\r\n"+
                        "! U1 SETBOLD 1 BS&W(/10%):! U1 SETBOLD 0  2\r\n"+
                        "! U1 SETBOLD 1 Turned On Time:! U1 SETBOLD 0  08/15/2014 09:30:53 AM\r\n"+
                        "! U1 SETBOLD 1 Turned Off Time:! U1 SETBOLD 0  08/15/2014 09:31:02 AM\r\n"+
                        "! U1 SETBOLD 1 Seal Off #:! U1 SETBOLD 0  5\r\n"+
                        "! U1 SETLF 7\r\n"+
                        "! U1 SETBOLD 1 Seal On #:! U1 SETBOLD 0  89\r\n"+
                        "! 0 200 200 5 1\r\n"+
                        "LINE 0 0 560 0 2\r\n"+
                        "PRINT\r\n"+
                        "! U1 SETLF 20\r\n"+
                        "! U1 SETBOLD 1 Tank/Meter #:! U1 SETBOLD 0  789\r\n"+
                        "! U1 SETBOLD 1 1st Reading:! U1 SETBOLD 0  4\r\n"+
                        "! U1 SETBOLD 1 2nd Reading:! U1 SETBOLD 0  5\r\n"+
                        "! U1 SETLF 40\r\n"+
                        "! U1 SETBOLD 1 Meter Factor:! U1 SETBOLD 0  1\r\n"+
                        "! U1 SETLF 15\r\n"+
                        "! U1 SETBOLD 1 Remarks:! U1 SETBOLD 0  45\r\n";
                        connection.write(cpclConfigLabel.getBytes());





                    }
                    connection.close();

                    if (file != null) {
                        file.delete();
                        file = null;
                    }
                } catch (ConnectionException e) {
                    helper.showErrorDialogOnGuiThread(e.getMessage());
                } catch (ZebraPrinterLanguageUnknownException e) {
                    helper.showErrorDialogOnGuiThread(e.getMessage());
                }
                //catch (ZebraIllegalArgumentException e) {
                //helper.showErrorDialogOnGuiThread(e.getMessage());
                //}
               finally {
                    bitmap.recycle();
                    helper.dismissLoadingDialog();
                    Looper.myLooper().quit();
                }
            }
        }).start();

    }

    private Connection getZebraPrinterConn() {
        int portNumber;
        try {
            portNumber = Integer.parseInt(getTcpPortNumber());
        } catch (NumberFormatException e) {
            portNumber = 0;
        }
        return isBluetoothSelected() ? new BluetoothConnection(getMacAddressFieldText()) : new TcpConnection(getTcpAddress(), portNumber);
    }

    private void getAndSaveSettings() {
        SettingsHelper.saveBluetoothAddress(ImagePrintDemo.this, getMacAddressFieldText());
        SettingsHelper.saveIp(ImagePrintDemo.this, getTcpAddress());
        SettingsHelper.savePort(ImagePrintDemo.this, getTcpPortNumber());
    }

}
